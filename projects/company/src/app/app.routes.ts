import {Routes} from '@angular/router';

export const appRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'moderation',
        loadChildren: () => import('./main/modules/moderation/moderation.module').then(m => m.ModerationModule)
      },
    ]
  },
];

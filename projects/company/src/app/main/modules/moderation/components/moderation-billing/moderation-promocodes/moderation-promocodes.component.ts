import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Promocode} from '../../../../../../../../../core/src/lib/entities/promocode';
import {PageEvent} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {ModerationPromocodeEditDialogComponent} from './moderation-promocode-edit-dialog/moderation-promocode-edit-dialog.component';
import {TranslateService} from '@ngx-translate/core';
import {DialogsService} from '../../../../../../../../../core/src/lib/services/dialogs.service';
import {PromocodesService} from '../../../../../../../../../core/src/lib/services/promocodes.service';

@Component({
  selector: 'app-moderation-promocodes',
  templateUrl: './moderation-promocodes.component.html',
  styleUrls: ['./moderation-promocodes.component.scss']
})
export class ModerationPromocodesComponent implements OnInit {
  dataSource: MatTableDataSource<Promocode> = null;
  displayedColumns = ['code', 'discount', 'description', 'expiresAt', 'usagesCount', 'usagesLimit', 'status', 'actions'];
  pageSizeOptions = [15, 30, 50];
  currentPage = 0;
  itemsPerPage = 15;
  numTotalItems = 0;

  constructor(private promocodesService: PromocodesService, private matDialog: MatDialog, private translateService: TranslateService,
              private dialogsService: DialogsService) { }

  ngOnInit(): void {
    this._loadPromocodes();
  }

  createPromocode(): void {
    this.matDialog.open(ModerationPromocodeEditDialogComponent).afterClosed().subscribe(promocodeId => {
      if (promocodeId) {
        this._loadPromocodes();
      }
    });
  }

  editPromocode(promocode: Promocode): void {
    this.matDialog.open(ModerationPromocodeEditDialogComponent, { data: { promocode } }).afterClosed().subscribe(promocodeId => {
      if (promocodeId) {
        this._loadPromocodes();
      }
    });
  }

  deletePromocode(promocode: Promocode): void {
    this.translateService.get('CONFIRM_PROMOCODE_DELETION').subscribe(t => {
      this.dialogsService.confirm(t).subscribe(confirmed => {
        if (confirmed) {
          this.promocodesService.deletePromocode(promocode.id).subscribe(res => {
            this._loadPromocodes();
          });
        }
      });
    });
  }

  handlePage(event?: PageEvent): PageEvent {
    this._loadPromocodes(event);
    return event;
  }

  private _loadPromocodes(event?: PageEvent): void {
    if (event) {
      this.currentPage = event.pageIndex;
      this.itemsPerPage = event.pageSize;
    }
    this.promocodesService.loadPromocodes(this.currentPage, this.itemsPerPage).subscribe(res => {
      this.dataSource = new MatTableDataSource<Promocode>(res.items);
      this.numTotalItems = res.totalCount;
    });
  }
}

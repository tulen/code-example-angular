import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import {CoreModule} from '../../../../../../core/src/lib/core.module';
import {routes} from './moderation.routes';
import {ModerationReviewsComponent} from './components/moderation-reviews/moderation-reviews.component';
import {ModerateReviewDialogComponent} from './components/moderation-reviews/moderate-review-dialog/moderate-review-dialog.component';
import {LightboxModule} from 'ngx-lightbox';
import {ModerationBloggersComponent} from './components/moderation-bloggers/moderation-bloggers.component';
import {ModerateBloggerDialogComponent} from './components/moderation-bloggers/moderate-blogger-dialog/moderate-blogger-dialog.component';
import {ModerationUsersComponent} from './components/moderation-users/moderation-users.component';
import {ModerationParserComponent} from './components/moderation-parser/moderation-parser.component';
import {ModerationParserAccountsComponent} from './components/moderation-parser/moderation-parser-accounts/moderation-parser-accounts.component';
import {ModerationWithdrawRequestsComponent} from './components/moderation-withdraw-requests/moderation-withdraw-requests.component';
import {ModerationTariffsComponent} from './components/moderation-billing/moderation-tariffs/moderation-tariffs.component';
import {ModerationTariffsEditDialogComponent} from './components/moderation-billing/moderation-tariffs/moderation-tariffs-edit-dialog/moderation-tariffs-edit-dialog.component';
import { ModerationPromocodesComponent } from './components/moderation-billing/moderation-promocodes/moderation-promocodes.component';
import { ModerationBillingComponent } from './components/moderation-billing/moderation-billing.component';
import { ModerationPromocodeEditDialogComponent } from './components/moderation-billing/moderation-promocodes/moderation-promocode-edit-dialog/moderation-promocode-edit-dialog.component';
import { ModerationUsersEditDialogComponent } from './components/moderation-users/moderation-users-edit-dialog/moderation-users-edit-dialog.component';
import { ModerationBannersComponent } from './components/moderation-banners/moderation-banners.component';
import { ModerationBannersEditDialogComponent } from './components/moderation-banners/moderation-banners-edit-dialog/moderation-banners-edit-dialog.component';
import { ModerationPaymentsComponent } from './components/moderation-billing/moderation-payments/moderation-payments.component';

@NgModule({
  declarations: [
    ModerationReviewsComponent,
    ModerateReviewDialogComponent,
    ModerationBloggersComponent,
    ModerateBloggerDialogComponent,
    ModerationUsersComponent,
    ModerationParserComponent,
    ModerationParserAccountsComponent,
    ModerationWithdrawRequestsComponent,
    ModerationTariffsComponent,
    ModerationTariffsEditDialogComponent,
    ModerationPromocodesComponent,
    ModerationBillingComponent,
    ModerationPromocodeEditDialogComponent,
    ModerationUsersEditDialogComponent,
    ModerationBannersComponent,
    ModerationBannersEditDialogComponent,
    ModerationPaymentsComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CoreModule,
    SharedModule,
    CommonModule,
    LightboxModule
  ]
})
export class ModerationModule {
}

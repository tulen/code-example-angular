import {Routes} from '@angular/router';
import {ModeratorGuard} from '../../../../../../core/src/lib/guards/moderator-guard.service';
import {ModerationBillingComponent} from './components/moderation-billing/moderation-billing.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [ModeratorGuard],
    children: [
      {
        path: 'billing',
        component: ModerationBillingComponent,
      },
    ]
  }
];

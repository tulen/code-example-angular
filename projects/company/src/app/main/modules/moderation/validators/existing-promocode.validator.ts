import { AbstractControl } from '@angular/forms';
import {debounceTime, delay, distinctUntilChanged, map, switchMap} from 'rxjs/operators';
import {of} from 'rxjs/internal/observable/of';
import {BillingService} from '../../../../../../../core/src/lib/services/billing.service';
import {PromocodesService} from '../../../../../../../core/src/lib/services/promocodes.service';

export class ExistingPromocodeValidator {
  static DEBOUNCE_TIME = 300;

  static createValidator(promocodesService: PromocodesService, promocodeId?: number) {
    let validatorFn = (control: AbstractControl) => {
      if (control.value === null || control.value === '' || !control.valueChanges || control.pristine) {
        return of(null);
      }
      return control.valueChanges.pipe(
        delay(ExistingPromocodeValidator.DEBOUNCE_TIME),
        debounceTime(ExistingPromocodeValidator.DEBOUNCE_TIME),
        distinctUntilChanged(),
        switchMap(value => promocodesService.validateExistingPromocode(value, promocodeId)),
        map(result => {
          control.setErrors(result ? null : { custom: { message: 'VALIDATION.PROMOCODE_ALREADY_EXISTS' } });
        })
      );
    };
    return validatorFn;
  }
}

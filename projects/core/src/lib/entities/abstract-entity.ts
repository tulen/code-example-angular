export interface IAbstractEntityData {
  id: number;
}

export abstract class AbstractEntity {
  static ORDER_IDX_FIRST = 10;
  static ORDER_IDX_STEP = 10;
  public id: number;
}

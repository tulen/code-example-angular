import {AbstractEntity, IAbstractEntityData} from './abstract-entity';

export enum EPromocodeStatus {
    disabled = 'disabled',
    active = 'active'
}

export interface IPromocodeData extends IAbstractEntityData {
  status: EPromocodeStatus;
  code: string;
  description: string;
  discount: number;
  expiresAt: number;
  usagesLimit: number;
  usagesCount: number;
  createdAt: number;
}

export class Promocode extends AbstractEntity {
  public status: EPromocodeStatus;
  public code: string;
  public description: string;
  public discount: number;
  public expiresAt: number;
  public usagesLimit: number;
  public usagesCount: number;
  public createdAt: number;

  public static factoryFromData(data: IPromocodeData): Promocode {
    const obj = new Promocode();
    obj.id = data.id;
    obj.status = data.status;
    obj.code = data.code;
    obj.description = data.description;
    obj.discount = data.discount;
    obj.expiresAt = data.expiresAt;
    obj.usagesLimit = data.usagesLimit;
    obj.usagesCount = data.usagesCount;
    obj.createdAt = data.createdAt;

    return obj;
  }

  get isStatusDisabled(): boolean {
    return this.status === EPromocodeStatus.disabled;
  }

  get isStatusActive(): boolean {
    return this.status === EPromocodeStatus.active;
  }
}

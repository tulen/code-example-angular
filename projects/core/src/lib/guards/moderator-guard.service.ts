import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ModeratorGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return new Observable<boolean>(subscribe => {
        this.authService.getCurrentUser().subscribe(user => {
            if (!this.authService.isAuthorized() || !user.isRoleModerator) {
                this.router.navigate(['/']);
                subscribe.next(false);
            }
            subscribe.next(true);
        });
    });
  }

}

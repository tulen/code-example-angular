import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {AuthService} from '../services/auth.service';
import {Observable} from 'rxjs/internal/Observable';
import {environment} from 'environments/environment';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
   constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.startsWith(environment.apiHost)) {
      const update = {params: req.params};

      const accessToken = this.authService.getAccessToken();
      if (accessToken) {
        update.params = req.params.set('access-token', accessToken);
      }

      const apiReq = req.clone(update);
      return next.handle(apiReq);
    }
    return next.handle(req);
  }
}

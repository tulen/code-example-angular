import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {environment} from 'environments/environment';
import {catchError, map} from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';

interface ApiResponseBase {
  status: string;
  message: string;
}

export interface ApiResponse<T> extends ApiResponseBase {
  result: T;
}

export interface ApiResponseItems<T> extends ApiResponseBase {
  result: {
    items: T[],
    _links: {
      self: {
        href: string
      }
    },
    _meta: {
      totalCount: number,
      pageCount: number,
      currentPage: number,
      perPage: number
    }
  };
}

export class ApiResultItems<T> {
  items: T[];
  totalCount: number;
  pageCount: number;
  currentPage: number;
  perPage: number;
}

export enum EApiResponseType { std, items }

@Injectable({
    providedIn: 'root'
})
export class ApiService {
  public static RESPONSE_STATUS_SUCCESS = 'success';
  public static RESPONSE_STATUS_ERROR = 'error';
  public static FIRST_PAGE = 0;
  public static ALL_ITEMS = 1000;

  public constructor(private httpClient: HttpClient, private matSnackBar: MatSnackBar, private translateService: TranslateService,
                     private router: Router) {

  }

  public post<T, U>(url: string, body: any|null, options: object|null, processResultCallback: (resultItem: T) => U): Observable<U> {
    const obs = this.httpClient.post<ApiResponseBase>(environment.apiHost + url, body, options);
    return this.processResponse<T, U>(obs, EApiResponseType.std, processResultCallback) as Observable<U>;
  }

  public put<T, U>(url: string, body: any|null, options: object|null, processResultCallback: (resultItem: T) => U): Observable<U> {
    const obs = this.httpClient.put<ApiResponseBase>(environment.apiHost + url, body, options);
    return this.processResponse<T, U>(obs, EApiResponseType.std, processResultCallback) as Observable<U>;
  }

  public delete<T, U>(url: string, options: object|null, processResultCallback: (resultItem: T) => U): Observable<U> {
    const obs = this.httpClient.delete<ApiResponseBase>(environment.apiHost + url, options);
    return this.processResponse<T, U>(obs, EApiResponseType.std, processResultCallback) as Observable<U>;
  }

  public get<T, U>(url: string, options: object|null, processResultCallback: (resultItem: T) => U): Observable<U> {
    const obs = this.httpClient.get<ApiResponseBase>(environment.apiHost + url, options);
    return this.processResponse<T, U>(obs, EApiResponseType.std, processResultCallback) as Observable<U>;
  }

  public getItems<T, U>(url: string, options: object, processResultCallback?: (resultItem: T) => U): Observable<ApiResultItems<U>> {
      const obs = this.httpClient.get<ApiResponseBase>(environment.apiHost + url, options);
      return this.processResponse<T, U>(obs, EApiResponseType.items, processResultCallback) as Observable<ApiResultItems<U>>;
  }

  protected processResponse<T, U>(responseObservable: Observable<ApiResponseBase>, responseType: EApiResponseType,
                                  processResultCallback?: (resultItem: T) => U): Observable<U|ApiResultItems<U>> {
    const obs = responseObservable.pipe(
      map(
        (response) => {
            if (response.status === ApiService.RESPONSE_STATUS_SUCCESS) {
                if (responseType === EApiResponseType.std) {
                    const res = response as ApiResponse<T>;
                    return this._convertResponseToValue<T, U>(res, processResultCallback);
                } else if (responseType === EApiResponseType.items) {
                    const res = response as ApiResponseItems<T>;
                    return this._convertResponseToItems<T, U>(res, processResultCallback);
                } else {
                  throw new Error('API error: Wrong response format');
                }
            } else {
              throw new Error(response.message);
            }
        }
      ),
      catchError((err: HttpErrorResponse): Observable<any> => {
        throw err;
      })
    );
    return obs;
  }

  private _convertResponseToValue<T, U>(response: ApiResponse<T>, processResultCallback: (result: T) => U): U {
    return processResultCallback(response.result);
  }

  private _convertResponseToItems<T, U>(response: ApiResponseItems<T>, processItemCallback?: (item: T) => U): ApiResultItems<U> {
    const result = new ApiResultItems<U>();
    if (response.result._meta) {
      result.totalCount = response.result._meta.totalCount;
      result.pageCount = response.result._meta.pageCount;
      result.currentPage = response.result._meta.currentPage;
      result.perPage = response.result._meta.perPage;
    } else {
      result.totalCount = response.result.items.length;
    }

    const items: U[] = [];
    for (const item of response.result.items) {
        items.push(processItemCallback(item));
    }
    result.items = items;

    return result;
  }
}

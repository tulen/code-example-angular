import {Injectable} from '@angular/core';
import {ApiResultItems, ApiService} from './api.service';
import {Observable} from 'rxjs/index';
import {HttpParams} from '@angular/common/http';
import {EPromocodeStatus, IPromocodeData, Promocode} from '../entities/promocode';

export interface IPromocodePersistenceData {
  status: EPromocodeStatus;
  code: string;
  discount: number;
  description: string;
  expiresAt: number;
  usagesLimit: number;
}

@Injectable({
  providedIn: 'root'
})
export class PromocodesService {
  constructor(private apiService: ApiService) {
  }

  public loadPromocodes(page: number, perpage: number): Observable<ApiResultItems<Promocode>> {
    const params = new HttpParams()
      .set('page', page.toString())
      .set('perpage', perpage.toString());
    return this.apiService.getItems<IPromocodeData, Promocode>('/promocodes', { params }, data => Promocode.factoryFromData(data));
  }

  public loadPromocodeByCode(code: string): Observable<Promocode> {
    return this.apiService.get<IPromocodeData, Promocode>('/promocodes/check/' + code, {}, data => Promocode.factoryFromData(data));
  }

  public createPromocode(persistenceData: IPromocodePersistenceData): Observable<number> {
    return this.apiService.post<number, number>('/promocodes', persistenceData, {}, data => data);
  }

  public updatePromocode(id: number, persistenceData: IPromocodePersistenceData): Observable<number> {
    return this.apiService.put<number, number>('/promocodes/' + id, persistenceData, {}, data => data);
  }

  public deletePromocode(id: number): Observable<void> {
    return this.apiService.delete<void, void>('/promocodes/' + id, {}, data => data);
  }

  public validateExistingPromocode(code: string, promocodeId?: number): Observable<boolean> {
    let params = new HttpParams();
    if (promocodeId) {
      params = params.set('promocodeId', promocodeId.toString());
    }
    return this.apiService.get<boolean, boolean>('/promocodes/validate-existing/' + code, { params }, data => data);
  }
}
